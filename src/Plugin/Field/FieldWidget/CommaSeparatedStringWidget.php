<?php

namespace Drupal\comma_separated_string_widget\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\StringTextfieldWidget;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'string_textfield' widget.
 *
 * @FieldWidget(
 *   id = "comma_separated_string_textfield",
 *   label = @Translation("Textfield (comma separated values)"),
 *   field_types = {
 *     "string"
 *   },
 *   multiple_values = TRUE
 * )
 */
class CommaSeparatedStringWidget extends StringTextfieldWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    if ($items->isEmpty()) {
      return $element;
    }
    $values = array_map(static function ($item): ?string {
      return $item->value ?? NULL;
    }, (array) $items->getIterator());
    $values = array_filter($values);
    $element['value']['#default_value'] = implode(', ', $values);
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    $values = parent::massageFormValues($values, $form, $form_state);
    // Explode the single value into multiple values.
    return static::massageInput($values['value'] ?? '');
  }

  /**
   * Massages the input of the form element.
   *
   * This method outputs the format expected from ::massageFormValues().
   *
   * @param string $input
   *   The text field contents as entered by the user.
   * @param string $delimiter
   *   The delimiter.
   *
   * @return array
   *   The output in the format expected from ::massageFormValues().
   */
  public static function massageInput(string $input, string $delimiter = ','): array {
    $items = array_map('trim', explode($delimiter, $input));
    $items = array_values(array_filter($items));
    return array_map(static function ($item) {
      return ['value' => $item];
    }, $items);
  }

}
