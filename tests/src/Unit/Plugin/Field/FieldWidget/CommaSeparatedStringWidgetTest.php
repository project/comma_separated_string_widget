<?php

namespace Drupal\Tests\comma_separated_string_widget\Unit\Plugin\Field\FieldWidget;

use Prophecy\PhpUnit\ProphecyTrait;
use ArrayObject;
use Drupal\comma_separated_string_widget\Plugin\Field\FieldWidget\CommaSeparatedStringWidget;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Tests\UnitTestCase;
use IteratorAggregate;
use Prophecy\Argument;

/**
 * Tests the CommaSeparatedStringWidget class.
 *
 * @coversDefaultClass \Drupal\comma_separated_string_widget\Plugin\Field\FieldWidget\CommaSeparatedStringWidget
 * @group comma_separated_string_widget
 *
 * @SuppressWarnings(PHPMD.StaticAccess)
 */
class CommaSeparatedStringWidgetTest extends UnitTestCase {

  use ProphecyTrait;
  /**
   * The system under test.
   *
   * @var \Drupal\comma_separated_string_widget\Plugin\Field\FieldWidget\CommaSeparatedStringWidget
   */
  private $widget;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $plugin_id = 'comma_separated_string_textfield';
    $plugin_definition = [];
    $field_definition = $this->prophesize(FieldDefinitionInterface::class);
    $settings = [];
    $third_party_settings = [];
    $this->widget = new CommaSeparatedStringWidget(
      $plugin_id,
      $plugin_definition,
      $field_definition->reveal(),
      $settings,
      $third_party_settings
    );
  }

  /**
   * Tests the massageFormValues method.
   *
   * @param string $user_input
   *   The provided user input.
   * @param array $expected
   *   The expected output values.
   *
   * @covers ::massageFormValues
   * @dataProvider massageFormValuesProvider
   */
  public function testMassageFormValues(string $user_input, array $expected): void {
    $form_state = $this->prophesize(FormStateInterface::class);
    $actual = $this->widget->massageFormValues(['value' => $user_input], [], $form_state->reveal());
    $this->assertArrayEquals($expected, $actual);
  }

  /**
   * Tests the massageInput method.
   *
   * @param string $user_input
   *   The provided user input.
   * @param array $expected
   *   The expected output values.
   *
   * @covers ::massageInput
   * @dataProvider massageFormValuesProvider
   */
  public function testMassageInput(string $user_input, array $expected): void {
    $this->assertArrayEquals(
      $expected,
      CommaSeparatedStringWidget::massageInput($user_input)
    );
  }

  /**
   * Data provider for testMassageFormValues.
   */
  public function massageFormValuesProvider(): array {
    return [
      [
        'first,second, third  ,	fourth',
        [
          ['value' => 'first'],
          ['value' => 'second'],
          ['value' => 'third'],
          ['value' => 'fourth'],
        ],
      ],
      ['', []],
      [',,,one,,one', [['value' => 'one'], ['value' => 'one']]],
    ];
  }

  /**
   * Tests the formElement method.
   *
   * @param array $values
   *   The stored values.
   * @param string $expected
   *   The expected default value.
   *
   * @covers ::formElement
   * @dataProvider formElementProvider
   */
  public function testFormElement(array $values, string $expected) {
    $items = $this->prophesize(FieldItemListInterface::class);
    $items->willImplement(IteratorAggregate::class);
    $item_values = array_map(function ($value) {
      return (object) ['value' => $value];
    }, $values);
    $array_object = new ArrayObject($item_values);
    $items->getIterator()->willReturn($array_object);
    $items->offsetExists(Argument::type('int'))->will(function ($args) use ($array_object) {
      return $array_object->offsetExists($args[0]);
    });
    $items->offsetGet(Argument::type('int'))->will(function ($args) use ($array_object) {
      return $array_object->offsetGet($args[0]);
    });
    $items->isEmpty()->willReturn(empty($values));
    $original_element = ['value' => []];
    $form = [];
    $form_state = $this->prophesize(FormStateInterface::class);
    $element = $this->widget->formElement($items->reveal(), random_int(0, 10), $original_element, $form, $form_state->reveal());
    if (empty($values)) {
      self::assertEmpty($element['value']['#default_value']);
    }
    else {
      self::assertSame($expected, $element['value']['#default_value']);
    }
  }

  /**
   * Data provider for testMassageFormValues.
   */
  public function formElementProvider(): array {
    return [
      [['one', 'two', 'three'], 'one, two, three'],
      [['one', FALSE, 'two', NULL, 'three'], 'one, two, three'],
      // The expected value is not used when the storage is empty.
      [[], $this->randomMachineName()],
      [[1], '1'],
    ];
  }

}
